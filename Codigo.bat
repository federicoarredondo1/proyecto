@echo off
SETLOCAL EnableDelayedExpansion

IF NOT EXIST Hospital (goto crearcarpetas)
set rango="Ninguno"
set rangoID=0
set CI=123456789
set Nombre="Ninguno"
set Apellido="Ninguno"
set SectorID=0
set SectorNombre="null"
set lastCreatedUser="null"

REM 0.000.0000
:login
set /p usuario="Ingrese su CI: "
set /p clave="Ingrese su clave: "
cd Hospital
cd Usuarios

IF EXIST "%usuario%.user" (
	find "%clave%" "%usuario%.user" >NUL || (
    	echo Clave incorrecta!
	    echo Presiona cualquier tecla para intentarlo de nuevo.
	    pause>nul
            cls
	    cd ..
	    cd ..
	    goto login
	)
	set /p Nombre=<Nombre/%usuario%.user
	set /p Apellido=<Apellido/%usuario%.user
	set CI=%usuario%
	set /p rangoID=<Rangos/%usuario%.user
	set /p SectorID=<SectorID/%usuario%.user
	cd ..
	IF "%rangoID%"=="1" (set rango="Paciente")
	IF "%rangoID%"=="2" (set rango="Administrativo")
	IF "%rangoID%"=="3" (set rango="Administrador")
	SETLOCAL DisableDelayedExpansion
	goto getSector
)
echo ¡El nombre de usuario es incorrecto!
echo Presiona cualquier tecla para intentarlo de nuevo.
pause>nul
cls
cd ..
cd ..
goto login

:menu
cls
IF %rangoID% EQU 1 (goto usermenu)
IF %rangoID% EQU 2 (goto hospmenu)
IF %rangoID% EQU 3 (goto adminmenu)
echo Ocurrio un error (E-001) (rangoID %rangoID%)
pause
exit

:usermenu
echo Hola %Nombre% %Apellido%
echo Tu cedula de identidad es %CI%
echo Actualmente te encuentras en el sector %SectorNombre%
pause>nul
cls
goto usermenu

:hospmenu
echo Buen dia %Nombre%
echo [1] - Crear paciente
echo [2] - Borrar paciente
echo [3] - Modificar paciente
echo [4] - Listar pacientes
echo [5] - Ver informacion de un paciente
echo [6] - Salir del programa
set /p opcion="Ingrese una opcion: "
cls
if "%opcion%"=="1" (goto crearuser)
if "%opcion%"=="2" (goto borraruser)
if "%opcion%"=="3" (goto edituser)
if "%opcion%"=="4" (goto listarusers)
if "%opcion%"=="5" (goto infopaciente)
if "%opcion%"=="6" (goto :EOF)
echo La opcion ingresada no es correcta.
echo Presione cualquier tecla para volver a mostrar el menu
pause>nul
goto hospmenu

:infopaciente
cls
set /p pacienteID="Ingrese la cedula del paciente: "
cd Usuarios
IF NOT EXIST "%pacienteID%.user" (
	echo Usuario invalido!
	echo Presione cualquier tecla para volver al menu
	pause>nul
	goto menu
)
set /p nombrea=<Nombre/%pacienteID%.user
set /p apellidoa=<Apellido/%pacienteID%.user
set /p rango=<Rangos/%pacienteID%.user
set /p sectoru=<SectorID/%pacienteID%.user
IF %rango% EQU 1 (set rankname=Paciente)
IF %rango% EQU 2 (set rankname=Administrativo)
IF %rango% EQU 3 (set rankname=Administrador)
IF %rango% EQU 3 (
    IF %rangoID% EQU 2 (
	    echo ¡No autorizado a ver esta informacion!
		echo Presione cualquier boton para salir
		pause>nul
		cd ..
		cls
		goto menu
	)
)
IF %sectoru% EQU 1 (set sectname=Hematologia)
IF %sectoru% EQU 2 (set sectname=Odontologia)
IF %sectoru% EQU 3 (set sectname=Pediatria)
IF %sectoru% EQU 4 (set sectname=Vacunaciones)
IF %sectoru% EQU 5 (set sectname=Consultorio Cardiologia)
IF %sectoru% EQU 6 (set sectname=Consultorio General)
IF %sectoru% EQU 7 (set sectname=Consultorio Neumologia)
IF %sectoru% EQU 8 (set sectname=Consultorio Neurologia)
IF %sectoru% EQU 9 (set sectname=CTI)
IF %sectoru% EQU 10 (set sectname=Operaciones)
IF %sectoru% EQU 11 (set sectname=UCI)
cls
echo Informacion de %nombrea% %apellidoa%
echo C.I.: %pacienteID%
echo Sector actual: %sectname% (ID: %sectoru%)
echo Rango: %rankname% (%rango%)
echo Presione cualquier tecla para volver al menu
cd ..
pause>nul
cls
goto menu

:dlr
cd Usuarios
set /p actualSector=<SectorID\%lastCreatedUser%.user
cd ..
IF %actualSector% EQU 1 (DEL /Q /F "PlantaBaja\Hematologia\%lastCreatedUser%.user")
IF %actualSector% EQU 2 (DEL /Q /F "PlantaBaja\Odontologia\%lastCreatedUser%.user")
IF %actualSector% EQU 3 (DEL /Q /F "PlantaBaja\Pediatria\%lastCreatedUser%.user")
IF %actualSector% EQU 4 (DEL /Q /F "PlantaBaja\Vacunaciones\%lastCreatedUser%.user")
IF %actualSector% EQU 5 (DEL /Q /F "PrimerPiso\Consultorios\Cardiologia\%lastCreatedUser%.user")
IF %actualSector% EQU 6 (DEL /Q /F "PrimerPiso\Consultorios\General\%lastCreatedUser%.user")
IF %actualSector% EQU 7 (DEL /Q /F "PrimerPiso\Consultorios\Neumologia\%lastCreatedUser%.user")
IF %actualSector% EQU 8 (DEL /Q /F "PrimerPiso\Consultorios\Neurologia\%lastCreatedUser%.user")
IF %actualSector% EQU 9 (DEL /Q /F "SegundoPiso\CTI\%lastCreatedUser%.user")
IF %actualSector% EQU 10 (DEL /Q /F "SegundoPiso\Operaciones\%lastCreatedUser%.user")
IF %actualSector% EQU 11 (DEL /Q /F "SegundoPiso\UCI\%lastCreatedUser%.user")
goto pisoselect

:listarusers
cls
IF %rangoID% EQU 2 (
	cd PlantaBaja
	tree /F /A > ArchivoTemp.txt
	more +4 "ArchivoTemp.txt" >"ArchivoTemp.txt.new"
	move /y "ArchivoTemp.txt.new" "ArchivoTemp.txt" >nul
	echo [ PLANTA BAJA ]
	type ArchivoTemp.txt
	DEL /Q /F ArchivoTemp.txt
	cd ..

	cd PrimerPiso
	cd Consultorios
	tree /F /A > ArchivoTemp.txt
	more +4 "ArchivoTemp.txt" >"ArchivoTemp.txt.new"
	move /y "ArchivoTemp.txt.new" "ArchivoTemp.txt" >nul
	echo [ PRIMER PISO ]
	type ArchivoTemp.txt
	DEL /Q /F ArchivoTemp.txt
	cd ..
	cd ..

	cd SegundoPiso
	tree /F /A > ArchivoTemp.txt
	more +4 "ArchivoTemp.txt" >"ArchivoTemp.txt.new"
	move /y "ArchivoTemp.txt.new" "ArchivoTemp.txt" >nul
	echo [ SEGUNDO PISO ]
	type ArchivoTemp.txt
	DEL /Q /F ArchivoTemp.txt
	cd ..

	echo Presione cualquier tecla para volver atras.
	pause>nul
	cls
	goto menu
)
cd Usuarios
cd Nombre
tree /F /A > ArchivoTemp.txt
more +3 "ArchivoTemp.txt" >"ArchivoTemp.txt.new"
move /y "ArchivoTemp.txt.new" "ArchivoTemp.txt" >nul
echo [ LISTADO DE USUARIOS ]
type ArchivoTemp.txt
DEL /Q /F ArchivoTemp.txt
echo Presione cualquier tecla para volver atras.
cd ..
cd ..
pause>nul
cls
goto menu

:edituser
echo [1] - Nombre
echo [2] - Apellido
echo [3] - Sector
IF %rangoID% EQU 3 (
	echo [4] - Rango
)
set /p opcion="Ingrese una opcion: "
IF "%opcion%"=="1" (goto editnombre)
IF "%opcion%"=="2" (goto editapellido)
IF "%opcion%"=="3" (goto editsector)
IF "%opcion%"=="4" (goto editrango)
echo Opcion invalida!
echo Presione cualquier tecla para volver al menu
pause>nul
goto menu

:editnombre
set /p nombreez="Ingrese la cedula del usuario: "
cd Usuarios
IF NOT EXIST "%nombreez%.user" (
	echo Usuario invalido!
	echo Presione cualquier tecla para volver al menu
	pause>nul
	goto menu
)
set /p newname="Ingrese el nuevo nombre: "
echo %newname% > Nombre/%nombreez%.user
echo Nombre editado, presione cualquier tecla para volver al menu
cd ..
pause>nul
goto menu

:editapellido
set /p nombreez="Ingrese la cedula del usuario: "
cd Usuarios
IF NOT EXIST "%nombreez%.user" (
	echo Usuario invalido!
	echo Presione cualquier tecla para volver al menu
	pause>nul
	goto menu
)
set /p newname="Ingrese el nuevo apellido: "
echo %newname% > Apellido/%nombreez%.user
echo Apellido editado, presione cualquier tecla para volver al menu
cd ..
pause>nul
goto menu

:editsector
set /p nombreez="Ingrese la cedula del usuario: "
cd Usuarios
IF NOT EXIST "%nombreez%.user" (
	echo Usuario invalido!
	echo Presione cualquier tecla para volver al menu
	pause>nul
	goto menu
)
cls
cd ..
set lastCreatedUser=%nombreez%
goto dlr

pause>nul

goto menu

:editrango
IF %rangoID% NEQ 3 (
	echo Solo administradores!
	pause
	goto menu
)
set /p nombreez="Ingrese la cedula del usuario: "
cd Usuarios
IF NOT EXIST "%nombreez%.user" (
	echo Usuario invalido!
	echo Presione cualquier tecla para volver al menu
	pause>nul
	goto menu
)
set /p newname="Ingrese el nuevo rango (ID): "
echo %newname% > Rangos/%nombreez%.user
echo Rango editado, presione cualquier tecla para volver al menu
pause>nul
cd ..
goto menu


:borraruser
set /p uzer="Ingrese la cedula del usuario (Ingrese 12345678 para volver atras):"
IF "%uzer%"=="12345678" (
	cls
	goto menu
)
IF NOT EXIST "./Usuarios/%uzer%.user" (
	echo Usuario invalido!
	echo Presione cualquier tecla para volver a ingresar.
	pause>nul
	cls
	goto borraruser
)

IF "%uzer%"=="%CI%" (
	echo No se puede borrar este usuario (R: Usuario actual)
	echo Presione cualquier tecla para ingresar otro.
	pause>nul
	cls
	goto borraruser
)
set lastCreatedUser=%uzer%
set /p conf="Confirmar operacion [Y/N]: "
IF "%conf%"=="Y" (goto borrarusersi)
IF "%conf%"=="y" (goto borrarusersi)

echo Operacion cancelada.
echo Presione cualquier tecla para volver al menu.
pause>nul
cls
goto menu

:borrarusersi
cd Usuarios
set uzer=%lastCreatedUser%
set rango=<Rango/%uzer%.user
IF %rango% NEQ 1 (
    IF %rangoID% EQU 2 (
    	echo No puedes borrar administrativos o administradores.
    	echo Presiona cualquier tecla para volver al menu.
    	pause > nul
    	cls
    	goto menu
    )
)
DEL /Q /F %uzer%.user
DEL /Q /F Rangos\%uzer%.user
DEL /Q /F Nombre\%uzer%.user
DEL /Q /F Apellido\%uzer%.user
set userSectorID=<SectorID/%uzer%.user
DEL /Q /F SectorID\%uzer%.user

IF "%userSectorID%"=="1" (
	DEL /Q /F ..\PlantaBaja\Hematologia\%uzer%.user
)
IF "%userSectorID%"=="2" (
	DEL /Q /F ..\PlantaBaja\Odontologia\%uzer%.user
)
IF "%userSectorID%"=="3" (
	DEL /Q /F ..\PlantaBaja\Pediatria\%uzer%.user
)
IF "%userSectorID%"=="4" (
	DEL /Q /F ..\PlantaBaja\Vacunaciones\%uzer%.user
)
IF "%userSectorID%"=="5" (
	DEL /Q /F ..\PlantaBaja\Consultorios\Cardiologia\%uzer%.user
)
IF "%userSectorID%"=="6" (
	DEL /Q /F ..\PlantaBaja\Consultorios\General\%uzer%.user
)
IF "%userSectorID%"=="7" (
	DEL /Q /F ..\PlantaBaja\Consultorios\Neumologia\%uzer%.user
)
IF "%userSectorID%"=="8" (
	DEL /Q /F ..\PlantaBaja\Consultorios\Neurologia\%uzer%.user
)
IF "%userSectorID%"=="9" (
	DEL /Q /F ..\SegundoPiso\CTI\%uzer%.user
)
IF "%userSectorID%"=="10" (
	DEL /Q /F ..\SegundoPiso\Operaciones\%uzer%.user
)
IF "%userSectorID%"=="11" (
	DEL /Q /F ..\SegundoPiso\UCI\%uzer%.user
)
echo Usuario eliminado completamente
echo Presione cualquier tecla para volver al menu.
pause>nul
cls
goto menu


:crearuser
set /p cedula="Ingrese la cedula del paciente (Ingrese 12345678 para volver atras): "
IF "%cedula%"=="12345678" (goto menu)
ECHO %cedula%> tempfile.user
FOR %%? IN (tempfile.user) DO ( SET /A strlength=%%~z? - 2 )
IF NOT "%strlength%"=="8" (
	echo La cedula ingresada es invalida.
	echo Presione cualquier tecla para volver a ingresar.
	pause>nul
	cls
	goto crearuser
)
DEL tempfile.user
set c1cedula=%cedula:~0,1%
set c2cedula=%cedula:~1,1%
set c3cedula=%cedula:~2,1%
set c4cedula=%cedula:~3,1%
set c5cedula=%cedula:~4,1%
set c6cedula=%cedula:~5,1% 
set c7cedula=%cedula:~6,1%
set c8cedula=%cedula:~7,1%
set /a cc1=%c1cedula%*8
set /a cc2=%c2cedula%*1
set /a cc3=%c3cedula%*2
set /a cc4=%c4cedula%*3
set /a cc5=%c5cedula%*4
set /a cc6=%c6cedula%*7
set /a cc7=%c7cedula%*6
set /a ccc=%cc1%+%cc2%+%cc3%+%cc4%+%cc5%+%cc6%+%cc7%
set ccc=%ccc:~-1%
set /a ccc=%ccc% %% 10
IF %ccc% NEQ %c8cedula% (
	echo La cedula ingresada es invalida.
	echo Presione cualquier tecla para volver a ingresar.
	pause>nul
	cls
	goto crearuser
)
IF EXIST /Usuarios/%cedula%.user (
	echo Este usuario ya existe.
	echo Presione cualquier tecla para reiniciar.
	pause>nul
	cls
    goto crearuser
)
set /p clave="Ingrese una clave para el usuario: "
set /p nomb="Ingrese el nombre del paciente: "
set /p apell="Ingrese el apellido del paciente: "
IF "%rangoID%"=="2" (set rank=1) 
IF "%rangoID%"=="3" (
	echo "1 - Paciente"
	echo "2 - Administrativo"
	echo "3 - Administrador"
	set /p rank="Ingrese la ID del rango: "
)

cd Usuarios
echo %clave% > %cedula%.user
echo %rank% > ./Rangos/%cedula%.user
echo %nomb% > ./Nombre/%cedula%.user
echo %apell% > ./Apellido/%cedula%.user
set lastCreatedUser=%cedula%
cd ..

echo Usuario %cedula% creado satisfactoriamente.
echo Debe asignarle un sector al usuario, presione cualquier tecla para hacerlo.
pause>nul
cls
goto pisoselect

:pisoselect
echo [1] - Planta baja
echo [2] - Primer piso
echo [3] - Segundo piso
set /p firstop="Eliga el piso: "
IF "%firstop%"=="1" (goto pbaja)
IF "%firstop%"=="2" (goto primerp)
IF "%firstop%"=="3" (goto segundop)
echo ¡Planta indicada incorrecta!
echo Presioen cualqueir tecla para ingresar otra.
pause>nul
cls
goto pisoselect

:pbaja
cd PlantaBaja
echo [1] - Hematologia
echo [2] - Odontologia
echo [3] - Pediatria
echo [4] - Vacunaciones
set /p elegida="Sector: "

IF "%elegida%"=="1" (
	echo Algun dato importante > Hematologia/%lastCreatedUser%.user
)
IF "%elegida%"=="2" (
	echo Algun dato importante > Odontologia/%lastCreatedUser%.user
)
IF "%elegida%"=="3" (
	echo Algun dato importante > Pediatria/%lastCreatedUser%.user
)
IF "%elegida%"=="4" (
	echo Algun dato importante > Vacunaciones/%lastCreatedUser%.user
)
echo %elegida% > ../Usuarios/SectorID/%lastCreatedUser%.user
echo Creacion del usuario finalizada.
echo Presione cualquier tecla para volver al menu
cd ..
pause>nul
goto menu

:primerp
cd PrimerPiso
echo [--Consultorios--]
echo [1] - Cardiologia
echo [2] - General
echo [3] - Neumologia
echo [4] - Neurologia
set /p elegida="Sector: "

IF "%elegida%"=="1" (
	echo Algun dato importante > Consultorios/Cardiologia/%lastCreatedUser%.user
)
IF "%elegida%"=="2" (
	echo Algun dato importante > Consultorios/General/%lastCreatedUser%.user
)
IF "%elegida%"=="3" (
	echo Algun dato importante > Consultorios/Neumologia/%lastCreatedUser%.user
)
IF "%elegida%"=="4" (
	echo Algun dato importante > Consultorios/Neurologia/%lastCreatedUser%.user
)
set /a sectorsito=%elegida%+4
echo %sectorsito% > ../Usuarios/SectorID/%lastCreatedUser%.user
echo Creacion del usuario finalizada.
echo Presione cualquier tecla para volver al menu
cd ..
pause>nul
goto menu

:segundop
cd SegundoPiso
echo [1] - CTI
echo [2] - Operaciones
echo [3] - UCI
set /p elegida="Sector: "

IF "%elegida%"=="1" (
	echo Algun dato importante > CTI/%lastCreatedUser%.user
)
IF "%elegida%"=="2" (
	echo Algun dato importante > Operaciones/%lastCreatedUser%.user
)
IF "%elegida%"=="3" (
	echo Algun dato importante > UCI/%lastCreatedUser%.user
)
set /a sectorsito=%elegida%+8
echo %sectorsito% > ../Usuarios/SectorID/%lastCreatedUser%.user
echo Sector del usuario creado/editado correctamente.
echo Presione cualquier tecla para volver al menu
cd ..
pause>nul
goto menu

:adminmenu
goto hospmenu

:getSector
IF %SectorID% EQU 1 (set SectorNombre=Hematologia)
IF %SectorID% EQU 2 (set SectorNombre=Odontologia)
IF %SectorID% EQU 3 (set SectorNombre=Pediatria)
IF %SectorID% EQU 4 (set SectorNombre=Vacunaciones)
IF %SectorID% EQU 5 (set SectorNombre=Consultorio Cardiologia)
IF %SectorID% EQU 6 (set SectorNombre=Consultorio General)
IF %SectorID% EQU 7 (set SectorNombre=Consultorio Neumologia)
IF %SectorID% EQU 8 (set SectorNombre=Consultorio Neurologia)
IF %SectorID% EQU 9 (set SectorNombre=CTI)
IF %SectorID% EQU 10 (set SectorNombre=Operaciones)
IF %SectorID% EQU 11 (set SectorNombre=UCI)
goto menu

:crearcarpetas
IF EXIST Hospital (goto menu)
mkdir Hospital
cd Hospital
mkdir PlantaBaja
cd PlantaBaja
mkdir Pediatria
mkdir Odontologia
mkdir Vacunaciones
mkdir Biblioteca
mkdir Informes
mkdir Hematologia
cd ..
mkdir PrimerPiso
cd PrimerPiso
mkdir Consultorios
cd Consultorios
mkdir General
mkdir Cardiologia
mkdir Neumologia
mkdir Neurologia
cd ..
cd ..
mkdir SegundoPiso
cd SegundoPiso
mkdir Operaciones
mkdir CTI
mkdir UCI
cd ..
mkdir Anexo
cd Anexo
mkdir SalaDeParto
mkdir Ginecologia
mkdir Prematuros
mkdir HogarDeMadres
mkdir Fisioterapia
cd Prematuros
mkdir CTI
mkdir "UCI 1"
mkdir "UCI 2"
mkdir SERENAR
mkdir SalaEstar
cd ..
cd ..
mkdir Anexo2
cd Anexo2
mkdir DNIC
mkdir RegistroNacimiento
mkdir Archivo
cd ..
mkdir Respaldo
mkdir Pacientes
mkdir Doctores
mkdir Usuarios
cd Usuarios
mkdir Apellido
mkdir Nombre
mkdir Rangos
mkdir SectorID
echo 3 > Rangos/Admin.user
echo Federico > Nombre/Admin.user
echo Arredondo > Apellido/Admin.user
echo 1234 > Admin.user
echo -1 > SectorID/Admin.user
cd ..
cls
echo Esta fue la primera ejecucion del programa, la carpeta con informacion fue creada.
echo Presione cualquier tecla para salir del programa.
pause > nul
exit

